$(document).ready(function(){
	$('.languages').find('.click_block').click(function(e){
		$(this).toggleClass('active');
		$(this).next().toggle();
        e.preventDefault();
	});
	$('.hidden_menu').hide();
	$('.show_menu').click(function(){
		$(this).next().fadeIn(300);
	});
	$('.hidden_menu .top').find('.icon_mdl').click(function(){
		$(this).parents('.hidden_menu').fadeOut(300);
	});
	$('.nav_popup').hide();
	$('.hidden_menu').find('.opens_popup.change').next().slideDown();
	$('.hidden_menu').find('.opens_popup').click(function(){
		$(this).toggleClass('change').next().slideToggle();
	});
	$('.hidden_menu').find('.opens_popup_second').click(function(){
		$(this).toggleClass('change').next().slideToggle();
	});
    $('ul.tabs').delegate('li:not(.current)', 'click', function() {
        $(this).addClass('current').siblings().removeClass('current')
            .parents('div.tabs_section').find('div.tab_info').hide().eq($(this).index()).fadeIn(150);
    });
    $('ul.tabs_low').delegate('li:not(.current)', 'click', function() {
        $(this).addClass('current').siblings().removeClass('current')
            .parents('div.section_low').find('div.box').hide().eq($(this).index()).fadeIn(150);
    });
    $('.upstair').click(function() {
	    $('html, body').animate({ scrollTop:0 }, 'slow');
	    return false;
	});

// Number function
    $('#number .minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('#number .plus').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });
// Number function:end
	$('.tab_accordion,.search_accordion').find('.hidden_list').hide();
	$('.tab_accordion,.search_accordion').find('h2').click(function(){
		$(this).next().slideToggle();
	})
});
